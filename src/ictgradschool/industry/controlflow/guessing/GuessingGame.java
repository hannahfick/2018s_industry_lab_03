package ictgradschool.industry.controlflow.guessing;

import ictgradschool.Keyboard;

import java.util.Scanner;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {
        System.out.println("Enter your guess (1 - 100): ");

        int randomnumber =((int) (Math.random() * 100));

        int goal = randomnumber;
        int guess = 0;

        while (guess != goal) {

            guess = Integer.parseInt(Keyboard.readInput());
            if (guess > goal) {

                System.out.println("Too high, try again");
            } else if (guess < goal) {
                System.out.println("Too low, try again");
            } else {
                System.out.println("Perfect!");
            }


        }


    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
