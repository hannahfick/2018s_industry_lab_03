package ictgradschool.industry.controlflow.coderunner;

import ictgradschool.Keyboard;

/**
 * Please run TestCodeRunner to check your answers
 */
public class CodeRunner {
    /**
     * Q1. Compare two names and if they are the same return "Same name",
     * otherwise if they start with exactly the same letter return "Same
     * first letter", otherwise return "No match".
     *
     * @param firstName
     * @param secondName
     * @return one of three Strings: "Same name", "Same first letter",
     * "No match"
     */
    public String areSameName(String firstName, String secondName) {
        String message = "";

        if (firstName.equals(secondName)) {
            message = "Same name";
        } else if (firstName.charAt(0) == secondName.charAt(0)) {
            message = "Same first letter";
        } else {
            message = "No match";
        }

        return message;
    }
/**
 * areSameName(String, String) => String
 * Q2. Determine if the given year is a leap year.
 *
 * @param year
 * @return true if the given year is a leap year, false otherwise
 */


    /**
     * Q2. Determine if the given year is a leap year.
     *
     * @param year
     * @return true if the given year is a leap year, false otherwise
     */
    public boolean isALeapYear(int year) {
        if (year % 4 == 0) {
            if ((year % 100 == 0) && (year % 400 != 0)) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }


/** isALeapYear(int) => boolean **/


    /**
     * Q3. When given an integer, return an integer that is the reverse (its
     * numbers are in reverse to the input).
     * order.
     *
     * @param number
     * @return the integer with digits in reverse order
     */
    public int reverseInt(int number) {
        int reverseNum = 0;
        int onesDigit;

        // Treat the input as a positive number while reversing
        int n = Math.abs(number);//It is the number variable to be checked for palindrome

        while (n > 0) {
            // Getting the least significant digit
            onesDigit = n % 10;  //getting remainder

            // Shift all numbers up one position (123 => 1230)
            // then add on the ones digit we just calculated
            reverseNum = (reverseNum * 10) + onesDigit;

            // Shift all numbers down one position (123 => 12)
            n = n / 10;
        }

        // If the input number was negative, the output should be also
        if (number < 0) {
            reverseNum = reverseNum * -1;
        }

        return reverseNum;
    }
/** reverseInt(int) => void **/


    /**
     * Q4. Return the given string in reverse order.
     *
     * @param str
     * @return the String with characters in reverse order
     */
    public String reverseString(String str) {
        String reverseStr = "";

        for (int i = str.length() - 1; i >= 0; i--) {
            reverseStr = reverseStr + str.charAt(i);
        }

        return reverseStr;
    }
/** reverseString(String) => void **/


    /**
     * Q5. Generates the simple multiplication table for the given integer.
     * The resulting table should be 'num' columns wide and 'num' rows tall.
     *
     * @param num
     * @return the multiplication table as a newline separated String
     */
    public String simpleMultiplicationTable(int num) {
        String multiplicationTable = "";

        for (int row = 1; row <= num; row++) {
            for (int col = 1; col <= num; col++) {
                multiplicationTable += (row * col) + " ";
            }

            multiplicationTable = multiplicationTable.trim();
            multiplicationTable += "\n";

        }
        String toReturn = multiplicationTable.trim();
        return toReturn;
    }
/** simpleMultiplicationTable(int) => void **/


    /**
     * Q6. Determines the Excel column name of the given column number.
     *
     * @param num
     * @return the column title as a String
     */
    public String convertIntToColTitle(int num) {
        String columnName = "";

        if (num <= 0){
            return "Input is invalid";
        }

        if (num > 26){
            // deal with the tens digit
            columnName += (char)((num / 26) + '@');
        }


        if (num % 26 == 0){
            columnName += "Z";
        } else {
            columnName += (char)((num % 26) + '@');
        }

        return columnName;
    }
/** convertIntToColTitle(int) => void **/


    /**
     * Q7. Determine if the given number is a prime number.
     *
     * @param num
     * @return true is the given number is a prime, false otherwise
     */
    public boolean isPrime(int num) {
        if (num == 2) {
            return true;
        }
        if (num < 2) {
            return false;
        }
        int max_factor = num / 2;
        for (int i = 2; i <= max_factor; i++) {
            if (num % i == 0) {
                return false;
            }
        }

        return true;
    }
/** isPrime(int) => void **/


    /**
     * Q8. Determine if the given integer is a palindrome (ignoring negative
     * sign).
     *
     * @param num
     * @return true is int is palindrome, false otherwise
     */
    public boolean isIntPalindrome(int num) {

        if (reverseInt(num) == num) {
            return true;
        } else
            return false;
    }


/** isIntPalindrom(int) => boolean **/


    /**
     * Q9. Determine if the given string is a palindrome (case folded).
     *
     * @param str
     * @return true if string is palindrome, false otherwise
     */
    public boolean isStringPalindrome(String str) {

        str = str.replaceAll("\\s", "");

        if (str.equals(reverseString(str))) {
            return true;
        } else {
            return false;
        }

    }
/** isStringPalindrome(String) => boolean **/


    /**
     * Q10. Generate a space separated list of all the prime numbers from 2
     * to the highest prime less than or equal to the given integer.
     *
     * @param num
     * @return the primes as a space separated list
     */
    public String printPrimeNumbers(int num) {
        String primesStr = "";

        for (int i = 2; i <= num; i++) {
            if (isPrime(i)) {
                primesStr += i + " ";
            }
        }
            if (primesStr.equals("")) {
                return "No prime number found";
            } else {
                return primesStr.trim();
            }
        }
    }